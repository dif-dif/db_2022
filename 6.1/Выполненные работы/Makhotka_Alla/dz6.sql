drop table if exists dialogue cascade;
drop table if exists messages cascade;

create table dialogue (
    id int generated always as identity primary key,
    start_time timestamptz,
    end_time timestamptz
);

create table messages (
    id int generated always as identity primary key,
    did int references dialogue not null,
    content text,
    send_time timestamptz,
    channel text,
    type text,
    check (type in ('question', 'answer'))
);

insert into dialogue (start_time, end_time)
values ('2022-02-05 14:20:01+03', '2022-02-05 14:25:45+03'),
       ('2022-04-01 06:09:01+03', '2022-02-05 06:20:10+03');

insert into messages (did, content, send_time, channel, type)
values (1, 'Яна, когда глава?', '2022-02-05 14:20:01+03', 'VK', 'question'),
       (1, 'ну она будет', '2022-02-05 14:20:59+03', 'VK', 'answer'),
       (1, 'Через полгода?', '2022-02-05 14:21:49+03', 'VK', 'question'),
       (1, 'говоришь так, будто это плохо', '2022-02-05 14:22:30+03', 'VK', 'answer'),
       (1, 'Это жестоко, а жестокость это плохо', '2022-02-05 14:23:07+03', 'VK', 'question'),
       (1, 'блин и правда((((', '2022-02-05 14:25:45+03', 'VK', 'answer'),
       (2, 'Ты проснулась?', '2022-04-01 06:09:01+03', 'Telegram', 'question'),
       (2, 'Проспала.', '2022-04-01 06:11:02+03', 'Telegram', 'answer'),
       (2, 'Придешь вообще?', '2022-04-01 06:12:23+03', 'Telegram', 'question'),
       (2, 'К обеду мб', '2022-04-01 06:17:17+03', 'Telegram', 'answer'),
       (2, 'Не вышла еще? Возьми колонку', '2022-04-01 06:18:41+03', 'Telegram', 'question'),
       (2, 'Я уже подхожу', '2022-04-01 06:20:10+03', 'Telegram', 'answer');

select *
from dialogue
    join messages on dialogue.id = messages.did
order by send_time;
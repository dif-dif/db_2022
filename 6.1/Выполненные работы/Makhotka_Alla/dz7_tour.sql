drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists city2tour cascade;

create table city (
    city_name text primary key ,
    founded text
);

create table review (
    id int generated always as identity primary key,
    comment text,
    rating int,
    tour_name text
);

create table tour (
    tour_name text primary key ,
    price int,
    city_name text references city
);

alter table review
    add foreign key (tour_name) references tour (tour_name);

create table city2tour (
    cityName text references city not null,
    tourName text references tour not null,
    unique (cityName, tourName)
);

insert into tour (tour_name, price)
values ('В гостях у хаски', 30000),
       ('Хиты Карелии', 40000);

insert into city (city_name, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);

insert into review (comment, rating, tour_name)
values ('Отличный тур!', 5, 'Хиты Карелии'),
       ('Не понравилось', 2, 'Хиты Карелии');

insert into city2tour (cityName, tourName)
values ('Приозерск', 'В гостях у хаски'),
       ('Сортавала', 'В гостях у хаски'),
       ('Сортавала', 'Хиты Карелии'),
       ('Валаам', 'Хиты Карелии');

-- select tour.tour_name, city.city_name, comment, rating
-- from tour
--     join city2tour on tour.tour_name = city2tour.tourName
--     join city on city.city_name = city2tour.cityName
--     left join review on review.tour_name = tour.tour_name;


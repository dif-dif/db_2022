drop table if exists users cascade;
drop table if exists twitts cascade;

create table users (
    id int generated always as identity primary key,
    name text,
    email text
);

create table twitts (
    id int generated always as identity primary key,
    uid int references users not null,
    content text,
    date timestamp
);

create table u2u (
    master_id int references users not null,
    slave_id int references users not null,
    unique (master_id, slave_id)
);

insert into users (name, email)
values ('dif', 'all.mahotka2012@yandex.ru'),
       ('undead', 'irusap54@gmail.com'),
       ('lolypop', 'lol.pop@gmail.com'),
       ('ekaterina1998', 'vorobey.em1998@mail.ru'),
       ('cheese', 'cheesycheese@gmail.com');

insert into twitts (uid, content, date)
values (2, 'And when we finally kill the gods neither ' ||
           'hell nor heaven will be waiting for them ' ||
           'because they created those to imprison us',
            '23-January-2022'),
       (2, 'me in my “i have to get an A” to “atleast ' ||
           'i submitted the paper” arc', '09-April-2022'),
       (3, 'I am, as the novelists say, in need of seaside ' ||
           'air', '07-March-2021'),
       (3, 'I actually reached enlightenment but it didnt ' ||
           'help LOL', '19-May-2022');

insert into u2u (master_id, slave_id)
values (1, 4),
       (1, 5),
       (2, 1),
       (3, 1);

select name, twitts.content
from users
	join u2u on (users.id = u2u.slave_id and u2u.master_id = 1)
	    or (users.id = u2u.master_id and u2u.slave_id = 1)
	left join twitts on twitts.uid = users.id;

-- Репозитории и юзеры: у юзера может быть несколько репозиториев,
-- в репозиторий могут контрибьютить много юзеров.
-- В репозитории может быть много тикетов.

drop table if exists users cascade;
drop table if exists repo cascade;
drop table if exists u2r cascade;
drop table if exists issues cascade;

create table users (
    id int generated always as identity primary key,
    username text not null,
    first_name text,
    last_name text,
    email text,
    year_born int
);

create table repo (
    id int generated always as identity primary key,
    uid int,
    name text,
    about text,
    type text,
    check (type in ('public', 'private')),
    clone_http text,
    clone_ssh text
);

create table u2r (
    user_id int references users not null,
    repo_id int references repo not null,
    unique (user_id, repo_id)
);

create table issues (
    iid int generated always as identity primary key,
    uid int references users,
    rid int references repo,
    issue_name text not null,
    content text,
    date timestamptz
);

insert into users (username, first_name, last_name, email)
values ('dif', 'Alla', 'Makhotka', 'all.mahotka2012@yandex.ru');

insert into repo (uid, name, about, type, clone_http, clone_ssh)
values (1, 'study', 'My repo', 'private',
        'https://github.com/dif-dif/study.git',
        'git@github.com:dif-dif/study.git');

insert into u2r (user_id, repo_id)
values (1, 1);

insert into issues (uid, rid, issue_name, content, date)
values (1, 1, 'New issue', 'I cant clone repo',
        '2022-02-05 14:20:01+03');

-- select iid, username, issue_name, content, date
-- from users
--     join issues i on users.id = i.uid
-- order by date;

-- select *
-- from repo

update repo
 set about = ''
    where uid = 5 and name = ''
    returning uid, name, about
from flask import Flask, jsonify, request
from psycopg2.extras import RealDictCursor
import psycopg2
from redis import Redis
import json

from deserialization import deserialize_holders

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

pg_conn = psycopg2.connect(database='postgres', user='postgres', password='changeme', host='localhost', port=999,
                           cursor_factory=RealDictCursor)
pg_conn.autocommit = True

redis_conn = Redis(port=26596, password='redis', decode_responses=True)


@app.route('/repos/')
def get_repos():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'repos:offset={offset},limit={limit}'
        redis_repos = redis_conn.get(redis_key)

        if redis_repos is None:
            cur = pg_conn.cursor()
            query = """
            select *
                from repo
            """

            cur.execute(query, (offset, limit))
            rows = cur.fetchall()
            cur.close()
            # holders = deserialize_holders(rows)

            redis_repos = json.dumps(rows, default=vars, ensure_ascii=False, indent=2)
            redis_conn.set(redis_key, redis_repos, ex=30)

        return redis_repos, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/repos/create', methods=['POST'])
def create_holder():
    try:
        body = request.json
        uid = body["uid"]
        name = body['name']
        about = body['about']
        type = body['type']
        clone_http = body['clone_http']
        clone_ssh = body['clone_ssh']

        cur = pg_conn.cursor()
        query = f"""
        insert into repo (uid, name, about, type, clone_http, clone_ssh)
        values (%s, %s, %s, %s, %s, %s)
        returning uid, name, about, type, clone_http, clone_ssh
        """

        cur.execute(query, (uid, name, about, type, clone_http, clone_ssh))
        result = cur.fetchall()
        cur.close()
        return {'message': f'Repo {result[0]["name"]} with description "{result[0]["about"]}" created.'}
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/repos/update', methods=['POST'])
def update_repo():
    try:
        body = request.json
        about = body['about']
        uid = body["uid"]
        name = body['name']


        cur = pg_conn.cursor()
        query = f"""
        update repo
        set about = %s
        where uid = %s and name = %s
        returning about, uid, name
        """

        cur.execute(query, (about, uid, name))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'Repo with name = "{name}" updated.'}
        else:
            return {'message': f'Repo with phone = "{name}" not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/repos/delete', methods=['DELETE'])
def delete_repo():
    try:
        body = request.json
        id = body['id']
        name = body['name']


        cur = pg_conn.cursor()
        query = f"""
        delete from repo
        where id = %s and name = %s
        returning id, name
        """

        cur.execute(query, (id, name))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'Repo with name = {name} deleted.'}
        else:
            return {'message': f'Repo with name = {name} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

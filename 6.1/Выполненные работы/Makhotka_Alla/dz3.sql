drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists city2tour cascade;

create table tour (
    id int generated always as identity primary key,
    name text,
    price int
);

create table city (
    id int generated always as identity primary key,
    name text,
    founded text
);

create table review (
    id int generated always as identity primary key,
    comment text,
    rating int,
    tour_name text
);

create table city2tour (
    cid int references city not null,
    tid int references tour not null,
    unique (cid, tid)
);

insert into tour (name, price)
values ('В гостях у хаски', 30000),
       ('Хиты Карелии', 40000);

insert into city (name, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);

insert into review (comment, rating, tour_name)
values ('Отличный тур!', 5, 'Хиты Карелии'),
       ('Не понравилось', 2, 'Хиты Карелии');

insert into city2tour (cid, tid)
values (1, 1),
       (2, 1),
       (2, 2),
       (3, 2);

select tour.name, city.name, comment, rating
from tour
    join city2tour on tour.id = city2tour.tid
    join city on city.id = city2tour.cid
    left join review on tour.name = review.tour_name;
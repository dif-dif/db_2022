import psycopg2
from psycopg2.extras import RealDictCursor
from typing import List
from pprint import pprint


connection = psycopg2.connect(database='postgres',
                        user='postgres',
                        password='changeme',
                        host='localhost',
                        port=999,
                        cursor_factory=RealDictCursor)


def citytour():
    class Tour:
        def __init__(self, name: str, price: float):
            self.id = id
            self.name = name
            self.price = price
            self.cities: List[City] = []
            self.reviews: List[Review] = []

    class City:
        def __init__(self, name: str, founded: str):
            self.id = id
            self.name = name
            self.founded = founded
            self.tours: List[Tour] = []

    class Review:
        def __init__(self, id: int, comment: str, rating: int):
            self.id = id
            self.comment = comment
            self.rating = rating
            self.tour_name: tour_name


    cursor = connection.cursor()

    query = """
    select tour.tour_name, city.city_name, comment, rating, 
    price, founded, id
    from tour
        join city2tour on tour.tour_name = city2tour.tourName
        join city on city.city_name = city2tour.cityName
        left join review on review.tour_name = tour.tour_name;

    """

    cursor.execute(query)
    rows = cursor.fetchall()

    # pprint(rows)
    
    tours_dict = {}
    cities_dict = {}
    reviews_dict = {}

    for row in rows:
        tour_name = row['tour_name']
        tour_price = row['price']

        tour = None
        if tour_name in tours_dict:
            tour = tours_dict[tour_name]
        else:
            tour = Tour(tour_name, tour_price)
            tours_dict[tour_name] = tour

        city_name = row['city_name']
        city_founded = row['founded']

        city = None
        if city_name in cities_dict:
            city = cities_dict[city_name]
        elif city_name is not None:
            city = City(city_name, city_founded)
            cities_dict[city_name] = city

        review_id = row['id']
        review_comment = row['comment']
        review_rating = row['rating']
        tour_name = row['tour_name']

        review = None
        if review_id in reviews_dict:
            review = reviews_dict[review_id]
        elif review_id is not None:
            review = Review(review_id, review_comment, review_rating)
            reviews_dict[review_id] = review

        if city_name is not None:
            if city not in tour.cities: tour.cities.append(city)
            if tour not in city.tours: city.tours.append(tour)

        if review is not None:
            if review not in tour.reviews: tour.reviews.append(review)
            review.tour = tour


    tours = list(tours_dict.values())
    cities = list(cities_dict.values())
    reviews = list(reviews_dict.values())
    # pprint(rows)

    return tours, cities, reviews

citytour()

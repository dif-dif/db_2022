import psycopg2
from pprint import pprint
from psycopg2.extras import RealDictCursor
from typing import List

connection = psycopg2.connect(database='dz7',
                        user='postgres',
                        password='changeme',
                        host='localhost',
                        port=999,
                        cursor_factory=RealDictCursor)

def hierarchy():
    class Folder:
        def __init__(self, id:int, name:str):
            self.id = id
            self.name = name
            self.size = None
            self.parent = None
            self.childrens: List[Folder] = []
            self.files: List[File] = []

    class File:
        def __init__(self, id:int, name:str, size:int):
            self.id = id
            self.name = name
            self.size = size
            self.folder: Folder

    cursor = connection.cursor()

    query = """
        select parent.id as folder_id, 
            parent.name as folder_name, 
            parent.par_id as folder_parent_id,
            child.id as child_id, 
            child.name as child_folder, 
            child.par_id as child_parent_id,
            file.id as file_id,
            file.fid as file_folder_id,
            file.name as file_name,
            file.size as file_size
        from folder as parent
            left join folder as child on parent.id = child.par_id
            left join file on parent.id = file.fid
        order by folder_id;
    """

    cursor.execute(query)
    rows = cursor.fetchall()
    
    # pprint(rows)

    folders_dict = {}
    files_dict = {}

    for row in rows:
        # print(rows[::-1].index(row))
        folder_id = row['folder_id']
        folder_name = row['folder_name']
        folder_parent_id = row['folder_parent_id']
        # print(folder_parent_id)
        file_id = row['file_id']
        file_name = row['file_name']
        file_size = row['file_size']
        file_folder = row['file_folder_id']
        
        folder = None
        if folder_id in folders_dict:
            folder = folders_dict[folder_id]
        else:
            folder = Folder(folder_id, folder_name)
            folders_dict[folder_id] = folder
            
        file = None
        if file_id in files_dict:
            file = files_dict[file_id]
        elif file_id is not None:
            file = File(file_id, file_name, file_size)
            files_dict[file_id] = file

        if folder_parent_id is not None:
            p_folder = folders_dict[folder_parent_id]
            if folder not in p_folder.childrens:
                p_folder.childrens.append(folder)
                folder.parent = p_folder

        if file_folder is not None:
            f_folder = folders_dict[file_folder]
            if file not in f_folder.files:
                f_folder.files.append(file)
                file.folder = f_folder

    folders = list(folders_dict.values())
    files = list(files_dict.values())
    # pprint(rows)
    
    return folders, files
    
hierarchy()

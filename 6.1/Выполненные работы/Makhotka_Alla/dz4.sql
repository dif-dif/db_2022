drop table if exists folder cascade;
drop table if exists file cascade;

create table folder (
    id int generated always as identity primary key,
    par_id int references folder,
    name text,
    unique (par_id, name)
);

create table file (
    id int generated always as identity primary key,
    fid int references folder,
    name text,
    size bigint,
    unique (fid, name)
);

insert into folder (par_id, name)
values (null, 'root'),
       (1, 'Documents'),
       (1, 'Downloads'),
       (2, 'books'),
       (3, 'Telegram Desktop');

insert into file (fid, name, size)
values (2, 'banner.txt', 4),
       (4, 'English for IT.pdf', 349),
       (5, 'picture(1).jpg', 200),
       (3, 'The.Batman.2022.1080p.WEB-DL.mkv', 65378);


select parent.name as folder,
       child.name as subfolder,
       file.name as file_name,
       file.size
from folder as parent
    left join folder as child on parent.id = child.par_id
    left join file on parent.id = file.fid;

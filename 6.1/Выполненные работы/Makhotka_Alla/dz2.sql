drop table if exists users cascade;
drop table if exists settings cascade;

create table users (
    id int generated always as identity primary key,
    nickname text unique,
    first_name text,
    last_name text
);

create table settings (
    uid int,
    font_size int,
    color_scheme text
);

alter table settings
    add foreign key (uid) references users(id);

alter table settings
    add unique (uid);

insert into users (nickname, first_name, last_name)
values ('dif', 'Alla', 'Makhotka'),
       ('irusap', 'Arseny', 'Borovoy');

insert into settings
values (1, 14, 'light'),
       (2, 11, 'dark');

select nickname, first_name, last_name, font_size, color_scheme
from users
    join settings on id = uid;
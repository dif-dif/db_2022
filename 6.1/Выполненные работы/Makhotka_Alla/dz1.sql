drop table if exists Tour cascade;
drop table if exists Review cascade;

create table Tour (
    name text primary key,
    price int
);

create table Review (
    id int generated always as identity primary key ,
    comment text,
    rating int,
    tour_name text references Tour
);

insert into Tour values ('Пушкинские горы', 20000),
                        ('Сочи', 30000);

insert into Review (comment, rating, tour_name) values ('Отличный тур', 5, 'Пушкинские горы'),
                          ('Ужасная организация', 1, 'Пушкинские горы'),
                          ('Детям понравилось', 4, 'Сочи'),
                          ('Не стоит своих денег', 3, 'Сочи');

select name as name_tour, comment, rating
from Tour
    left join Review on tour_name = name;

-- drop table Tour cascade;
-- drop table Review cascade;
